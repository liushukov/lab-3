## Лабораторна робота 3

### Тема: "Дослідження методів багатопоточності та засобів паралельного API Java при розробки програмних компонентів розподіленної системі. Робота з пакетом java.util.concurrent"

#### Виконав: Люшуков Кирило  

### Завдання 1
## 1) Постановка завдання
![Скріншот01](https://gitlab.com/liushukov/lab-3/-/raw/main/images/task1.1.png?ref_type=heads)
![Скріншот02](https://gitlab.com/liushukov/lab-3/-/raw/main/images/task1.2.png?ref_type=heads)
![Скріншот03](https://gitlab.com/liushukov/lab-3/-/raw/main/images/task1.3.png?ref_type=heads)

## 2) Реалізація
![Скріншот04](https://gitlab.com/liushukov/lab-3/-/raw/main/images/main.png?ref_type=heads)
**клас Main**
---
![Скріншот05](https://gitlab.com/liushukov/lab-3/-/raw/main/images/Mutex1.png?ref_type=heads)
![Скріншот06](https://gitlab.com/liushukov/lab-3/-/raw/main/images/Mutex2.png?ref_type=heads)
**клас MutexProgram**
---
![Скріншот07](https://gitlab.com/liushukov/lab-3/-/raw/main/images/Semaphore1.png?ref_type=heads)
![Скріншот08](https://gitlab.com/liushukov/lab-3/-/raw/main/images/Semaphore2.png?ref_type=heads)
**клас SemaphoreProgram**
---
![Скріншот09](https://gitlab.com/liushukov/lab-3/-/raw/main/images/Blocking1.png?ref_type=heads)
![Скріншот010](https://gitlab.com/liushukov/lab-3/-/raw/main/images/Blocking2.png?ref_type=heads)
**клас BlockingVariableProgram**
---
![Скріншот011](https://gitlab.com/liushukov/lab-3/-/raw/main/images/Pools1.png?ref_type=heads)
![Скріншот012](https://gitlab.com/liushukov/lab-3/-/raw/main/images/Pools2.png?ref_type=heads)
**клас ThreadPoolSimulation**
---
## 3) Результати виконання
![Скріншот013](https://gitlab.com/liushukov/lab-3/-/raw/main/images/Result1.png?ref_type=heads)
![Скріншот014](https://gitlab.com/liushukov/lab-3/-/raw/main/images/Result2.png?ref_type=heads)
![Скріншот015](https://gitlab.com/liushukov/lab-3/-/raw/main/images/Result3.png?ref_type=heads)
![Скріншот016](https://gitlab.com/liushukov/lab-3/-/raw/main/images/Result4.png?ref_type=heads)
---


### Завдання 2  

#### Варіант № 14

## 1) Постановка завдання
![Скріншот1](https://gitlab.com/liushukov/lab-3/-/raw/main/images/photo1.1.png?ref_type=heads)
![Скріншот2](https://gitlab.com/liushukov/lab-3/-/raw/main/images/photo1.2.png?ref_type=heads)
---
## 2) Реалізація  
![Скріншот3](https://gitlab.com/liushukov/lab-3/-/raw/main/images/photo2.1.png?ref_type=heads)
**клас Main**
![Скріншот4](https://gitlab.com/liushukov/lab-3/-/raw/main/images/photo2.2.png?ref_type=heads)
**клас Storage**
![Скріншот5](https://gitlab.com/liushukov/lab-3/-/raw/main/images/photo2.3.png?ref_type=heads)
**клас Client**
![Скріншот6](https://gitlab.com/liushukov/lab-3/-/raw/main/images/photo2.4.png?ref_type=heads)
![Скріншот7](https://gitlab.com/liushukov/lab-3/-/raw/main/images/photo2.5.png?ref_type=heads)
**клас Barber**
---
## 3) Результати виконання
![Скріншот8](https://gitlab.com/liushukov/lab-3/-/raw/main/images/photo3.1.png?ref_type=heads)
**результат №1**
![Скріншот9](https://gitlab.com/liushukov/lab-3/-/raw/main/images/photo3.2.png?ref_type=heads)
**результат №2**
![Скріншот10](https://gitlab.com/liushukov/lab-3/-/raw/main/images/photo3.3.png?ref_type=heads)
**результат №3**
---