import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        Barber barber = new Barber();
        ExecutorService executorService = Executors.newCachedThreadPool();

        // барбер буде спати допоки клієнт його не розбудить
        executorService.submit(barber::goSleep);
        // імітація приходу 10 клієнтів
        for (int i = 1; i <= 10; i++) {
            executorService.submit(new Client(barber, "Client " + i));
            try {
                TimeUnit.MILLISECONDS.sleep(new Random().nextInt(500));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        executorService.shutdown();
    }
}
