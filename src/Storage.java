import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class Storage<T> {
    private final Queue<T> queue;

    public Storage(int capacity) {
        this.queue = new LinkedBlockingQueue<>(capacity);
    }
    public void add(T item) {
        queue.add(item);
    }

    public void poll() {
        queue.poll();
    }

    public T peek() {
        return queue.peek();
    }

    public int size() {
        return queue.size();
    }
    public boolean isEmpty() {
        return queue.isEmpty();
    }
}
